# Shopping List Planner

Shopping List Planner jest prostą aplikacją oparta na GUI, która pozwala użytkownikom zarządzać listami zakupów. Aplikacja jest zbudowana na potrzeby zajęć Inżynieria Oprogramowania.

![](src/main/resources/shoppinglistplanner.png)

## Wymagania funkcjionalne i niefunkcjonalne

**Wymagania funkcjonalne** 

|lp|    Nazwa        |  Opis          |
|---|---|---|
|1.|"New list"       |	Aktywuje możliwość dodania nowej listy	          |
|2.|"Add"		    | 	Realizuje funkcje dodania nowej listy / zapisania edytowanej| 
|3.|"Edit"			|   Włącza edycje danej listy|
|4.|"Delete"         |   Usuwa wybrana listę|
|5.|"Display"        |   Wyświetla wybrana listę|

**Wymagania niefunkcjonalne** 

   |lp|Opis|
   |---|---|
   |1.|Interface użytkownika |
   |2.|Kontrola wersji przy użyciu git'a| 	
   |3.|Testy jednostkowe|  
   |4.|Bezpieczeństo użytkowania , zablokowane niedozwolone działania| 

## Diagram przypadków użycia
![](src/main/resources/usagediagram.png)

## Diagram klas

![](src/main/resources/classdiagram.png)